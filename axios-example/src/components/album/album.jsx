import React, { Component } from 'react';

class Album extends Component {
    render() {
        return (
            <div className="col-md-4 mt-3">
                <div className="card">
                    <div className="card-header text-right">
                        <span className="pointer-cursor" onClick={this.props.onDelete}>x</span>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">{this.props.title}</h5>
                    </div>
                    <div className="card-footer text-center">
                        <a href={'photo/' + this.props.userId} title="Photos" className="btn btn-info btn-sm ml-2" type="button">
                            <i className="fa fa-picture-o"></i>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Album;