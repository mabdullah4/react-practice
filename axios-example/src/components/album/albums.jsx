import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AlbumAPI from '../../model/Album';
import Album from './album';

class Albums extends Component {
    state = {
        albums: [],
        albumAPI: new AlbumAPI()
    }
    userId
    params = this.props.match.params;

    componentDidMount() {
        this.state.albumAPI.getAlbums(this.params.userId, (result) => this.setState({ albums: result }));
    }

    handleDelete(id) {
        this.state.albumAPI.deleteAlbum(id, (result) => {
            const albums = this.state.albums.filter(album => album.id !== id)
            this.setState({ albums })
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <Link to="/" className="btn btn-primary mx-2 my-4">Home</Link>
                </div>
                <div className="row mb-4">
                    {
                        this.state.albums.map(album => {
                            return <Album key={album.id} userId={album.userId} title={album.title} onDelete={() => this.handleDelete(album.id)} />
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Albums;