import React, { Component } from 'react';

class Post extends Component {
    render() {
        return (
            <div className="col-md-4 mt-3">
                <div className="card">
                    <div className="card-header text-right">
                        <span className="pointer-cursor" onClick={this.props.onDelete}>x</span>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">{this.props.title}</h5>
                        <p className="card-text">{this.props.description}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Post;