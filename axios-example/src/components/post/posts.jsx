import React, { Component } from 'react';
import PostAPI from '../../model/Post';
import Post from './post';

import { Link } from 'react-router-dom';

class Posts extends Component {
    state = {
        posts: [],
        postAPI: new PostAPI()
    }

    params = this.props.match.params;

    componentDidMount() {
        this.state.postAPI.getPosts(this.params.userId, (result) => this.setState({ posts: result }));
    }

    handleDelete(id) {
        this.state.postAPI.deletePost(id, (result) => {
            const posts = this.state.posts.filter(post => post.id !== id)
            this.setState({ posts })
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <Link to="/" className="btn btn-primary mx-3 my-2">Home</Link>
                </div>
                <div className="row mb-4">
                    {
                        this.state.posts.map(post => {
                            return <Post key={post.id} title={post.title} description={post.body} onDelete={() => this.handleDelete(post.id)} />
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Posts;