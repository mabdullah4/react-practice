import React, { Component } from 'react';
import User from './user';

import UserAPI from './../../model/User';

class Users extends Component {
    state = {
        userAPI: new UserAPI(),
        users: []
    }

    componentDidMount() {
        this.state.userAPI.getUsers((users) => {
            this.setState({ users })
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="btn-group" role="group" aria-label="Button group">
                        <button className="btn btn-primary" type="button">05</button>
                        <button className="btn btn-primary" type="button">10</button>
                    </div>
                </div>
                <div className="row mb-2">
                    {
                        this.state.users.map(user => {
                            return <User key={user.id} user={user} />
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Users;