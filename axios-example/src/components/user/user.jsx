import React from 'react';

const User = ({ user }) => {
    return (
        <div className="col-md-4 mt-3">
            <div className="card">
                <div className="card-header">
                    {user.name}
                </div>
                <div className="card-body">
                    <p className="card-text">UserName : {user.username}</p>
                    <p className="card-text">Email : <a href={'mailto:' + user.email}>{user.email}</a></p>
                    <p className="card-text">Phone : <a href={'tel:' + user.phone}>{user.phone}</a></p>
                    <p className="card-text">Website : <a target='_blank' href={'http://' + user.website}>{user.website}</a></p>
                    <p className="card-text">Address : {user.address.street + ',' + user.address.suite + '' + user.address.city}</p>
                </div>
                <div className="card-footer text-center">
                    <a href={'post/' + user.id} title="Posts" className="btn btn-success btn-sm" type="button">
                        <i className="fa fa-podcast"></i>
                    </a>
                    <a href={'todo/' + user.id} title="Todo" className="btn btn-primary btn-sm ml-2" type="button">
                        <i className="fa fa-check"></i>
                    </a>
                    <a href={'album/' + user.id} title="Albums" className="btn btn-info btn-sm ml-2" type="button">
                        <i className="fa fa-music"></i>
                    </a>
                </div>
            </div>
        </div>
    );
}

export default User;