import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import TodoAPI from '../../model/Todo';
import Todo from './todo';

class Todos extends Component {
    state = {
        todos: [],
        todoAPI: new TodoAPI()
    }
    params = this.props.match.params;

    componentDidMount() {
        this.state.todoAPI.getTodos(this.params.userId, (result) => this.setState({ todos: result }));
    }

    handleDelete(id) {
        this.state.todoAPI.deleteAlbum(id, (result) => {
            const todos = this.state.todos.filter(todo => todo.id !== id)
            this.setState({ todos })
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <Link to="/" className="btn btn-primary mx-2 my-4">Home</Link>
                </div>
                <div className="row mb-4">
                    {
                        this.state.todos.map(todo => {
                            return <Todo key={todo.id} completed={todo.completed} userId={todo.userId} title={todo.title} onDelete={() => this.handleDelete(todo.id)} />
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Todos;