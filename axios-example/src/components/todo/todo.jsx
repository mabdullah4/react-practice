import React, { Component } from 'react';

class Todo extends Component {
    render() {
        return (
            <div className="col-md-4 mt-3">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{this.props.title}</h5>
                    </div>
                    <div className="card-footer text-center">
                        {this.props.completed ? "Completed" : "Not Completed"}
                    </div>
                </div>
            </div>
        );
    }
}

export default Todo;