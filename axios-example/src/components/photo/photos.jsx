import React, { Component } from 'react';
import PhotoAPI from '../../model/Photo';
import Photo from './photo';

class Photos extends Component {
    state = {
        photos: [],
        photoAPI: new PhotoAPI()
    }

    params = this.props.match.params;

    componentDidMount() {
        this.state.photoAPI.getPhotos(this.params.userId, (result) => this.setState({ photos: result }));
    }

    handleDelete(id) {
        this.state.photoAPI.deletePhoto(id, (result) => {
            const photos = this.state.photos.filter(post => post.id !== id)
            this.setState({ photos })
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    {
                        this.state.photos.map(photo => {
                            return <Photo key={photo.id} title={photo.title} thumbnailUrl={photo.thumbnailUrl} onDelete={() => this.handleDelete(photo.id)} />
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Photos;