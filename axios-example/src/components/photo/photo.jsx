import React, { Component } from 'react';

class Photo extends Component {
    render() {
        return (
            <div className="col-md-4 mt-3">
                <div className="card">
                    <div className="card-header text-right">
                        {this.props.title}
                    </div>
                    <img className="card-img-top" src={this.props.thumbnailUrl} alt={this.props.title} />
                </div>
            </div>
        );
    }
}

export default Photo;