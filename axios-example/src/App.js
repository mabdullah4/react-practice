import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Users from './components/user/users';
import Posts from './components/post/posts';
import Photos from './components/photo/photos';
import Albums from './components/album/albums';
import Todos from './components/todo/todos';

class App extends Component {
  render() {
    return (
      <Router history="">
        <div className="App">
          <Route exact path="/" component={Users} />
          <Route exact path="/post/:userId" component={Posts} />
          <Route exact path="/todo/:userId" component={Todos} />
          <Route exact path="/album/:userId" component={Albums} />
          <Route exact path="/album/photo/:userId" component={Photos} />
        </div>
      </Router>
    );
  }
}

export default App;
