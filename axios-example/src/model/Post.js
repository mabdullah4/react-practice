import API from './../api/axios';

class Post {
    getPosts(userId, callback) {
        API.get(`posts?userId=${userId}`).then(callback).catch(callback)
    }

    addPost(data, callback) {
        API.post('/posts', data).then(callback).catch(callback)
    }

    deletePost(id, callback) {
        API.delete(`posts/${id}`).then(callback).catch(callback)
    }
}

export default Post;