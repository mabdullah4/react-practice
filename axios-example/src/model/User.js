import API from '../api/axios';

class User {
    getUsers(callback) {
        API.get('users').then(callback).catch(callback)
    }

    addUser(data, callback) {
        API.post('/users', data).then(callback).catch(callback)
    }

    deleteUser(id, callback) {
        API.delete(`users/${id}`).then(callback).catch(callback)
    }
}

export default User;