import API from '../api/axios';

class Album {
    getAlbums(userId, callback) {
        API.get(`albums?userId=${userId}`).then(callback).catch(callback)
    }

    addAlbum(data, callback) {
        API.post('/albums', data).then(callback).catch(callback)
    }

    deleteAlbum(id, callback) {
        API.delete(`albums/${id}`).then(callback).catch(callback)
    }
}

export default Album;