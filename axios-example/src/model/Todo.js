import API from '../api/axios';

class Todo {
    getTodos(userId, callback) {
        API.get(`todos?userId=${userId}`).then(callback).catch(callback)
    }

    addTodo(data, callback) {
        API.post('/todos', data).then(callback).catch(callback)
    }

    deleteTodo(id, callback) {
        API.delete(`todos/${id}`).then(callback).catch(callback)
    }
}

export default Todo;