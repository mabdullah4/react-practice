import API from '../api/axios';

class Photo {
    getPhotos(albumId, callback) {
        API.get(`photos?userId=${albumId}`).then(callback).catch(callback)
    }

    addPhoto(data, callback) {
        API.post('/photos', data).then(callback).catch(callback)
    }

    deletePhoto(id, callback) {
        API.delete(`photos/${id}`).then(callback).catch(callback)
    }
}

export default Photo;