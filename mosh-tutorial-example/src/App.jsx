import React, { Component } from 'react';

import Navbar from './components/navbar';
import Counters from './components/counters';

class App extends Component {
    state = {
        counters: [
            { id: 0, value: 4 },
            { id: 1, value: 3 },
            { id: 2, value: 2 },
            { id: 3, value: 1 },
        ]
    }

    handleReset = () => {
        const counters = this.state.counters.map(counter => {
            counter.value = 0;
            return counter;
        });
        this.setState({ counters })
    }

    handleIncrement = (counter) => {
        const counters = [...this.state.counters];
        const index = counters.indexOf(counter);
        counters[index] = { ...counter }
        counters[index].value++;
        this.setState({ counters });
    }

    handleDelete = (counterId) => {
        const counters = this.state.counters.filter(counter => counter.id !== counterId)
        this.setState({ counters });
    }


    render() {
        return (
            <React.Fragment>
                <Navbar total={this.state.counters.filter(c => c.value > 0).length} />
                <Counters
                    counters={this.state.counters}
                    onReset={this.handleReset}
                    onDelete={this.handleDelete}
                    onIncrement={this.handleIncrement}
                />
            </React.Fragment>
        );
    }
}

export default App;