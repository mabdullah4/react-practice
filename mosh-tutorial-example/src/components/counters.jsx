import React from 'react';

import Counter from './counter';

const Counters = (props) => {
    return (
        <div className="p-4">
            <button className="btn btn-danger btn-sm" onClick={props.onReset}>Reset</button>
            {
                props.counters.map(counter => (
                    <Counter key={counter.id} counter={counter} value={counter.value} onIncrement={() => props.onIncrement(counter)} onDelete={() => props.onDelete(counter.id)} />
                ))
            }
        </div>
    );
}

export default Counters;