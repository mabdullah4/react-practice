import React from 'react';

const Navbar = ({ total }) => {
    return (
        <div>
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
                <a className="navbar-brand">Brand
                    <span className="badge badge-primary badge-sm">{total}</span>
                </a>
            </nav>
        </div>
    );
}

export default Navbar;