import React, { Component } from 'react';

class Counter extends Component {

    render() {
        return (
            <div>
                <span className={this.getBadgeClasses()}>{this.props.counter.value}</span>
                <button className="btn btn-secondary btn-sm ml-4" onClick={this.props.onIncrement}>Increment</button>
                <button className="btn btn-danger btn-sm ml-4" onClick={this.props.onDelete}>Delete</button>
            </div>
        );
    }

    getBadgeClasses() {
        let classes = 'badge m-2  badge-';
        classes += this.props.counter.value === 0 ? 'warning' : 'primary';
        return classes;
    }
}

export default Counter;